<?php
// list_events.php
require_once(__DIR__."/../bootstrap.php");

$eventRepository = $em->getRepository('Event');
$events = $eventRepository->findAll();

foreach ($events as $event) {
    echo sprintf("-%s\n", $event->getName());
}