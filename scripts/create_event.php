<?php
// create_user.php
require_once "bootstrap.php";

$newEventname = $argv[1];

$event = new Event();
$event->setName($newEventname);
$event->setEventId(123456);
$event->setDescription("Some Description text");

$em->persist($event);
$em->flush();

echo "Created Event with ID " . $event->getId() . "\n";