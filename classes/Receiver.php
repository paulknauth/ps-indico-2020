<?php

namespace Classes;

use GuzzleHttp\Client;

class Receiver
{
    protected $client;

    protected $ak;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://testserv.knauth.digital/']);
        $this->ak = '0c153706-1835-47c7-a8b8-fe024f40aab0';
    }
    
    public function getClient()
    {
        return $this->client;
    }

    public function getEvents()
    {
        $response = $this->client->request('GET', 'lpdm/export/event?ak='.$this->ak);
        $result = (json_decode($response->getBody(), true));
        return $result;
    }

    public function getRegistrationForms($eventId)
    {
        if(is_int($eventId))
        {
            $eventId = strval($eventId);
        }
        $response = $this->client->request('GET', 'lpdm/export/event/'.$eventId.'/registration-forms?ak='.$this->ak);
        $result = (json_decode($response->getBody(), true));
        return $result;

    }

    public function getRegistrations($eventId)
    {
        if(is_int($eventId))
        {
            $eventId = strval($eventId);
        }
        $response = $this->client->request('GET', 'lpdm/export/event/'.$eventId.'/registrations?ak='.$this->ak);
        $result = (json_decode($response->getBody(), true));
        return $result['data'];

    }

    public function getContacts($eventId)
    {
        if(is_int($eventId))
        {
            $eventId = strval($eventId);
        }
        $response = $this->client->request('GET', 'lpdm/export/event/'.$eventId.'/contacts?ak='.$this->ak);
        $result = (json_decode($response->getBody(), true));
        return $result['data'];

    }

    public function getRegFormRegs($eventId, $regFormId)
    {
        if(is_int($eventId))
        {
            $eventId = strval($eventId);
        }

        if(is_int($regFormId))
        {
            $regFormId = strval($regFormId);
        }

        $response = $this->client->request('GET', 'lpdm/export/event/'.$eventId.'/registration-form/'.$regFormId.'/registrations?ak='.$this->ak);
        $result = (json_decode($response->getBody(), true));
        return $result['data'];
    }

    public function getRegFormCons($eventId, $regFormId)
    {
        if(is_int($eventId))
        {
            $eventId = strval($eventId);
        }

        if(is_int($regFormId))
        {
            $regFormId = strval($regFormId);
        }

        $response = $this->client->request('GET', 'lpdm/export/event/'.$eventId.'/registration-form/'.$regFormId.'/contacts?ak='.$this->ak);
        $result = (json_decode($response->getBody(), true));
        return $result['data'];
    }

    public function getEventInfo($eventId)
    {
        if(is_int($eventId))
        {
            $eventId = strval($eventId);
        }
        $response = $this->client->request('GET', 'export/event/'.$eventId.'.json?ak='.$this->ak);
        $result = (json_decode($response->getBody(), true));
        return $result['results'][0];

    }
}