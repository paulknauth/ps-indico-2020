<?php

namespace Classes\Repositories;

use Doctrine\ORM\EntityRepository;

class EventRepository extends EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e FROM Event e ORDER BY e.name ASC'
            )
            ->getResult();
    }
}