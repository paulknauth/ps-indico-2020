<?php

namespace Classes;


use Classes\Receiver;
use Classes\Handler;
use Event;
use RegistrationForm;

class Loader{

    protected $receiver;

    protected $em;
    protected $EventHandler;
    protected $ContactHandler;
    protected $RegistrationFormHandler;
    protected $RegistrationHandler;
    protected $UserHandler;

    public function __construct()
    {
        require_once(__DIR__."/../bootstrap.php");
        $this->receiver = new Receiver;
        $this->em = $em;
        $this->EventHandler = new Handler\EventHandler($this->em);
        $this->ContactHandler = new Handler\ContactHandler($this->em);
        $this->RegistrationFormHandler = new Handler\RegistrationFormHandler($this->em);
        $this->RegistrationHandler = new Handler\RegistrationHandler($this->em);
        $this->UserHandler = new Handler\UserHandler($this->em);
    }
    
    public function getEntityManager()
    {
        return $this->em;
    }
    
    public function listAllEvents()
    {
        $eventRepository = $this->em->getRepository('Event');
        $events = $eventRepository->findAll();

        if (empty($events)) {
            echo "No event found.\n";
            exit(1);
        }

        foreach ($events as $event) {
            echo sprintf("-%s\n", $event->getName());
        }
    }

    public function loadEvents()
    {
        $events = $this->receiver->getEvents();
        foreach ($events as $event) {
            $oldEvent = $this->em->getRepository('Event')->findBy(array('eventId' => $event['id']));
            if(empty($oldEvent)){
                $this->EventHandler->createEvent($event);
            }
            else {
                $this->EventHandler->updateEvent($event, $oldEvent[0]);
            }
            $currEvent = $this->em->getRepository('Event')->findBy(array('eventId' => $event['id']));
            $this->enhanceEventInfo($currEvent[0]);
        }
        unset($event);
    }

    public function enhanceEventInfo(Event $event)
    {
        $response = $this->receiver->getEventInfo($event->getEventId());
        $this->EventHandler->updateEventInfo($event, $response);
    }

    public function loadRegistrationForms(Event $event)
    {
        $eventId = $event->getEventId();
        $regForms = $this->receiver->getRegistrationForms($eventId);
        foreach ($regForms as $regForm) {
            $oldRegForm = $this->em->getRepository('RegistrationForm')->findBy(array('registrationFormId' => $regForm['id']));
            if(empty($oldRegForm)){
                $this->RegistrationFormHandler->createRegistrationForm($regForm, $event);
            }
            else {
                $this->RegistrationFormHandler->updateRegistrationForm($regForm, $oldRegForm[0], $event);
            }
        }
        unset($regForm);
    }

    public function loadRegistrations(Event $event, RegistrationForm $regForm)
    {
        $eventId = $event->getEventId();
        $regFormId = $regForm->getRegistrationFormId();
        $regs = $this->receiver->getRegFormRegs($eventId, $regFormId);
        foreach ($regs as $reg) {
            $oldReg = $this->em->getRepository('Registration')->findBy(array('email' => $reg['personal_data']['email'], 'registrationForm' => $regForm));
            if(empty($oldReg)){
                $this->RegistrationHandler->createRegistration($reg, $event, $regForm);
            }
            else {
                $this->RegistrationHandler->updateRegistration($reg, $oldReg[0], $event, $regForm);
            }
        }
        unset($reg);
    }

    public function loadContacts(Event $event, RegistrationForm $regForm)
    {
        $eventId = $event->getEventId();
        $regFormId = $regForm->getRegistrationFormId();
        $cons = $this->receiver->getRegFormCons($eventId, $regFormId);
        foreach ($cons as $con) {
            $oldCon = $this->em->getRepository('Contact')->findBy(array('email' => $con['email'], 'registrationForm' => $regForm));
            if(empty($oldCon)){
                $this->ContactHandler->createContact($con, $event, $regForm);
            }
            else {
                $this->ContactHandler->updateContact($con, $oldCon[0], $event, $regForm);
            }
        }
        unset($con);
    }

    public function loadUsers()
    {

    }

    public function loadAll()
    {
        $this->loadEvents();
        $eventRepository = $this->em->getRepository('Event');
        $events = $eventRepository->findAll();
        echo "Events succesfully loaded \n";

        foreach($events as $event){
            $this->loadRegistrationForms($event);
        }
        echo "RegistrationForms succesfully loaded \n";

        $regFormRepository = $this->em->getRepository('RegistrationForm');
        $regForms = $regFormRepository->findAll();
        foreach($regForms as $regForm){
            $regFormEvent = $regForm->getEvent();
            $this->loadRegistrations($regFormEvent, $regForm);
            $this->loadContacts($regFormEvent, $regForm);
        }
        echo "Registrations and Contacts loaded \n";
        $regs = $this->em->getRepository('Registration')->findAll();
        foreach($regs as $reg){
            $this->UserHandler->storeUserFromRegistration($reg);
        }
        echo "User <-> Registration Relation updated \n";

        $cons = $this->em->getRepository('Contact')->findAll();
        foreach($cons as $con){
            $this->UserHandler->storeUserFromContact($con);
        }
        echo "User <-> Contact Relation updated \n";
        echo "Everything loaded, inspect DataBase at localhost:5432 \n";
    }

    public function deleteAll()
    {
        $this->ContactHandler->deleteAllContacts();
        $this->RegistrationHandler->deleteAllRegistrations();
        $this->UserHandler->deleteAllUsers();
        $this->RegistrationFormHandler->deleteAllRegistrationForms();
        $this->EventHandler->deleteAllEvents();
        
    }

}