<?php

namespace Classes\Handler;

use Event;

class EventHandler
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }
    
    public function deleteAllEvents()
    {
        $eventRepository = $this->em->getRepository('Event');
        $events = $eventRepository->findAll();
        foreach ($events as $event)
        {
            $this->em->remove($event);
        }
        $this->em->flush();
    }

    public function createEvent($eventArray)
    {
        $event = new Event();
        $event->setName($eventArray['name']);
        $event->setEventId($eventArray['id']);
        $event->setDescription($eventArray['description']);
        $this->em->persist($event);
        $this->em->flush();
    }

    public function updateEvent($eventArray, $oldEvent)
    {
        $oldEvent->setName($eventArray['name']);
        $oldEvent->setEventId($eventArray['id']);
        $oldEvent->setDescription($eventArray['description']);
        $this->em->persist($oldEvent);
        $this->em->flush();
    }  
    
    public function updateEventInfo(Event $event, $eventArray)
    {
        $event->setLocation($eventArray['location']);
        $event->setAdress($eventArray['address']);
    }
}