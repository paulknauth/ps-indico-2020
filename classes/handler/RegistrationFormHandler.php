<?php

namespace Classes\Handler;

use RegistrationForm;
use Event;

class RegistrationFormHandler
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function deleteAllRegistrationForms()
    {
        $registrationFormRepository = $this->em->getRepository('RegistrationForm');
        $registrationForms = $registrationFormRepository->findAll();
        foreach ($registrationForms as $registrationForm)
        {
            $this->em->remove($registrationForm);
        }
        $this->em->flush();
    }

    public function createRegistrationForm($regFormArray, Event $event)
    {
        $regForm = new RegistrationForm;
        $regForm->setRegistrationFormId($regFormArray['id']);
        $regForm->setTitle($regFormArray['title']);
        $regForm->setEvent($event);
        $this->em->persist($regForm);
        $this->em->flush();
    }

    public function updateRegistrationForm($regFormArray, $oldRegForm, Event $event)
    {
        $oldRegForm->setTitle($regFormArray['title']);
        $oldRegForm->setRegistrationFormId($regFormArray['id']);
        $oldRegForm->setEvent($event);
        $this->em->persist($oldRegForm);
        $this->em->flush();
    }
}