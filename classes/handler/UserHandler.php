<?php

namespace Classes\Handler;

use Contact;
use Event;
use RegistrationForm;
use Registration;
use User;

class UserHandler
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }
    public function deleteAllUsers()
    {
        $userRepository = $this->em->getRepository('User');
        $users = $userRepository->findAll();
        foreach ($users as $user)
        {
            $this->em->remove($user);
        }
        $this->em->flush();
    }

    public function createUserFromContact(Contact $con)
    {
        $user = new User;
        $user->setEmail($con->getEmail());
        $user->setFirstName($con->getFirstName());
        $user->setSurName($con->getSurName());
        $user->setTitle($con->getTitle());
        $user->setPhone($con->getPhone());
        $con->setUser($user);
        $this->em->persist($con);
        $this->em->persist($user);
        $this->em->flush();
    }

    public function createUserFromRegistration(Registration $reg)
    {
        $user = new User;
        $user->setEmail($reg->getEmail());
        $user->setFirstName($reg->getFirstName());
        $user->setSurName($reg->getSurName());
        $user->setTitle($reg->getTitle());
        $user->setPhone($reg->getPhone());
        $reg->setUser($user);
        $this->em->persist($reg);
        $this->em->persist($user);
        $this->em->flush();
    }

    public function updateUserFromContact(Contact $con,User $oldUser)
    {
        $oldUser->setEmail($con->getEmail());
        $oldUser->setFirstName($con->getFirstName());
        $oldUser->setSurName($con->getSurName());
        $oldUser->setTitle($con->getTitle());
        $oldUser->setPhone($con->getPhone());
        $con->setUser($oldUser);
        $this->em->persist($oldUser);
        $this->em->persist($con);
        $this->em->flush();
    }

    public function updateUserFromRegistration(Registration $reg,User $oldUser)
    {
        $oldUser->setEmail($reg->getEmail());
        $oldUser->setFirstName($reg->getFirstName());
        $oldUser->setSurName($reg->getSurName());
        $oldUser->setTitle($reg->getTitle());
        $oldUser->setPhone($reg->getPhone());
        $reg->setUser($oldUser);
        $this->em->persist($oldUser);
        $this->em->persist($reg);
        $this->em->flush();
    }

    public function storeUserFromContact(Contact $con)
    {
       $email = $con->getEmail();
       $users = $this->em->getRepository('User')->findBy(array('email' => $email));
       if(empty($users)){
           $this->createUserFromContact($con);
       }
       else{$this->updateUserFromContact($con, $users[0]);}
    }

    public function storeUserFromRegistration(Registration $reg) 
    {
       $email = $reg->getEmail();
       $users = $this->em->getRepository('User')->findBy(array('email' => $email));
       if(empty($users)){
           $this->createUserFromRegistration($reg);
       }
       else{$this->updateUserFromRegistration($reg, $users[0]);}
    }
}