<?php

namespace Classes\Handler;

use Registration;
use Event;
use RegistrationForm;

class RegistrationHandler
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }
    public function deleteAllRegistrations()
    {
        $registrationRepository = $this->em->getRepository('Registration');
        $registrations = $registrationRepository->findAll();
        foreach ($registrations as $registration)
        {
            $this->em->remove($registration);
        }
        $this->em->flush();
    }

    public function createRegistration($regArray, Event $event, RegistrationForm $regForm)
    {
        $reg = new Registration;
        $reg->setRegId($regArray['registrant_id']);
        $reg->setEmail($regArray['personal_data']['email']);
        $reg->setFirstName($regArray['personal_data']['firstName']);
        $reg->setSurName($regArray['personal_data']['surname']);
        $reg->setTitle($regArray['personal_data']['title']);
        $reg->setPhone($regArray['personal_data']['phone']);
        $reg->setEvent($event);
        $reg->setRegistrationForm($regForm);
        $this->em->persist($reg);
        $this->em->flush();
    }

    public function updateRegistration($regArray, $oldReg, Event $event, RegistrationForm $regForm)
    {
        $oldReg->setEmail($regArray['personal_data']['email']);
        $oldReg->setFirstName($regArray['personal_data']['firstName']);
        $oldReg->setSurName($regArray['personal_data']['surname']);
        $oldReg->setTitle($regArray['personal_data']['title']);
        $oldReg->setPhone($regArray['personal_data']['phone']);
        $oldReg->setRegId($regArray['registrant_id']);
        $oldReg->setEvent($event);
        $oldReg->setRegistrationForm($regForm);
        $this->em->persist($oldReg);
        $this->em->flush();
    }
}