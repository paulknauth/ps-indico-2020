<?php

namespace Classes\Handler;

use Contact;
use Event;
use RegistrationForm;

class ContactHandler
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }
    public function deleteAllContacts()
    {
        $contactRepository = $this->em->getRepository('Contact');
        $contacts = $contactRepository->findAll();
        foreach ($contacts as $contact)
        {
            $this->em->remove($contact);
        }
        $this->em->flush();
    }

    public function createContact($conArray, Event $event, RegistrationForm $regForm)
    {
        $con = new Contact;
        $con->setEmail($conArray['email']);
        $con->setFirstName($conArray['firstName']);
        $con->setSurName($conArray['surname']);
        $con->setTitle($conArray['title']);
        $con->setPhone($conArray['phone']);
        $con->setEvent($event);
        $con->setRegistrationForm($regForm);
        $this->em->persist($con);
        $this->em->flush();
    }

    public function updateContact($conArray, $oldCon, Event $event, RegistrationForm $regForm)
    {
        $oldCon->setEmail($conArray['email']);
        $oldCon->setFirstName($conArray['firstName']);
        $oldCon->setSurName($conArray['surname']);
        $oldCon->setTitle($conArray['title']);
        $oldCon->setPhone($conArray['phone']);
        $oldCon->setEvent($event);
        $oldCon->setRegistrationForm($regForm);
        $this->em->persist($oldCon);
        $this->em->flush();
    }
}