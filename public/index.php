<?php
// public/index.php

require(__DIR__.('/../vendor/autoload.php'));
use Classes\Receiver;
use Classes\Loader;
use Classes\Repositories\EventRepository;

$receiver = new Receiver;
$loader = new Loader;
$em = $loader->getEntityManager();
$client = $receiver->getClient();
echo "Testing API by loading eventlist from Indico: \n";
$response = $receiver->getEvents();
print_r($response);
echo "API Test succesful \n";
$loader->loadAll();