#Setup

To start the project, you need to enter a few commands right now:

`docker-compose up --build`

In some Cases:
`docker-compose composer composer install`
`docker-compose composer dump-autload -o`

Initialize db Schema: 

`docker-compose exec php php vendor/bin/doctrine orm:schema-tool:create`

Update db Schema:

`docker-compose exec php php vendor/bin/doctrine orm:schema-tool:update --force`

Drop db Schema:

`docker-compose exec php php vendor/bin/doctrine orm:schema-tool:drop --force`

# Connect to DataBase via DataGrip:

* Host: localhost
* Port: 5432
* Database Name: dbname
* User Name: dbuser
* Password: dbpwd