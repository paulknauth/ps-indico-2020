<?php
// src/User.php

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity 
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Id 
     * @ORM\GeneratedValue 
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     * @ORM\OneToMany(targetEntity="Registration", mappedBy="user")
     * @var ownedRegistrations[] An ArrayCollection of Registration objects.
     */
    protected $ownedRegistrations;

    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="user")
     * @var ownedContacts[] An ArrayCollection of Contact objects.
     */
    protected $ownedContacts;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function __construct()
    {
        $this->ownedRegistrations = new ArrayCollection();
        $this->ownedContacts = new ArrayCollection();
    }
    
    public function addOwnedRegistration(Registration $reg)
    {
        $this->ownedRegistrations[] = $reg;
    }

    public function addOwnedContact(Contact $reg)
    {
        $this->ownedContacts[] = $reg;
    }

    public function setFirstName($firstName)
    {
        $this->firstname = $firstName;
    }

    public function getFirstName()
    {
        return $this->firstname;
    }

    public function setSurName($surName)
    {
        $this->surname = $surName;
    }

    public function getSurName()
    {
        return $this->surname;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getPhone()
    {
        return $this->phone;
    }
}