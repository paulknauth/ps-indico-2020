<?php
// src/Event.php

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="events")
 */
class Event
{
    /**
     * @ORM\Id 
     * @ORM\Column(type="integer") 
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", unique=true, nullable=false)
     */
    protected $eventId;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $adress;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $location;

    /**
     * @ORM\OneToMany(targetEntity="RegistrationForm", mappedBy="event")
     */
    protected $ownedRegistrationForms;

    /**
     * @ORM\OneToMany(targetEntity="Registration", mappedBy="event")
     */
    protected $ownedRegistrations;

    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="event")
     */
    protected $ownedContacts;

    public function __construct()
    {
        $this->ownedRegistrationForms = new ArrayCollection();
        $this->ownedRegistration = new ArrayCollection();
        $this->ownedContacts = new ArrayCollection();
    }

    public function addOwnedRegistrationForm(RegistrationForm $registrationForm)
    {
        $this->ownedRegistrationForms[] = $registrationForm;
    }

    public function addOwnedRegistration(Registration $registration)
    {
        $this->ownedRegistration[] = $registration;
    }

    public function addOwnedContact(Contact $contact)
    {
        $this->ownedContacts[] = $contact;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getEventId()
    {
        return $this->eventId;
    }

    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function getAdress()
    {
        return $this->adress;
    }

    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

}