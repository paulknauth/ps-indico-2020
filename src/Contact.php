<?php
// src/Bug.php

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="contacts")
 */
class Contact
{
    /**
     * @ORM\Id 
     * @ORM\Column(type="integer") 
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     * @ORM\ManyToOne(targetEntity="RegistrationForm", inversedBy="ownedContacts")
     */
    protected $registrationForm;

    /**
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="ownedContacts")
     */
    protected $event;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ownedContacts")
     */
    protected $user;

    public function setRegistrationForm(RegistrationForm $registrationForm)
    {
        $registrationForm->addOwnedContact($this);
        $this->registrationForm = $registrationForm;
    }

    public function getRegistrationForm()
    {
        return $this->registrationForm;
    }

    public function setEvent(Event $event)
    {
        $event->addOwnedContact($this);
        $this->event = $event;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function setUser(User $user)
    {
        $user->addOwnedContact($this);
        $this->user= $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setFirstName($firstName)
    {
        $this->firstname = $firstName;
    }

    public function getFirstName()
    {
        return $this->firstname;
    }

    public function setSurName($surName)
    {
        $this->surname = $surName;
    }

    public function getSurName()
    {
        return $this->surname;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getPhone()
    {
        return $this->phone;
    }

}