<?php
// src/RegistrationForm.php

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="registrationForms")
 */
class RegistrationForm
{
    /**
     * @ORM\Id 
     * @ORM\Column(type="integer") 
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", unique=true, nullable=false)
     */
    protected $registrationFormId;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\OneToMany(targetEntity="Registration", mappedBy="registrationForm")
     */
    protected $ownedRegistrations;

    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="registrationForm")
     */
    protected $ownedContacts;

    /**
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="ownedRegistrationForms")
     */
    protected $event;

    public function __construct()
    {
        $this->ownedRegistrations = new ArrayCollection();
        $this->ownedContacts = new ArrayCollection();
    }

    public function setEvent(Event $event)
    {
        $event->addOwnedRegistrationForm($this);
        $this->event = $event;
    }

    public function addOwnedRegistration(Registration $registration)
    {
        $this->ownedRegistrations[] = $registration;
    }

    public function addOwnedContact(Contact $contact)
    {
        $this->ownedContacts[] = $contact;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRegistrationFormId()
    {
        return $this->registrationFormId;
    }

    public function setRegistrationFormId($registrationFormId)
    {
        $this->registrationFormId = $registrationFormId;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getRegistrations()
    {
        return $this->ownedRegistrations;
    }

    public function getContacts()
    {
        return $this->ownedContacts;
    }

}